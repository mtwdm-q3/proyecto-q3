export default () => {
    return {
        express: require('debug')('api:Express'),
        socketIO: require('debug')('api:Socket.IO'),
        mongoDB: require('debug')('api:MongoDB'), 
        nodeJS: require('debug')('api:NodeJS'),

        unhandleError: require('debug')('api:Unhandle Error')
    }
}