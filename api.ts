
// console.log('Server funcionando', process.env.NODE_ENV);

// Settings Import
import Settings from './settings';
// Import Color Util
import ColorUtilLib from './utils/color.util';
// Import Debug Util
import DebugUtilLib from './utils/debug.util';
// Import Mongo Helper
import MongoHelper from './helpers/mongo.helper';
// Import Express
import Express from 'express';
// Import Express FileUpload
// import fileUpload from 'express-fileupload';
// Import Express Rate Limit
import rateLimit from 'express-rate-limit';
// Import Demo Routes
import DemoRoute from './routes/demo.route';
// Import Hospital Routes 
import HospitalRoute from './routes/hospital.route';
// Import CORS
import cors from 'cors';
// Import Morgan
import morgan from 'morgan';
// Import Helmet Hardenning Express
import helmet from 'helmet';
// Import Compression
import compression from "compression";
// Import Swagger
import swaggerUI from 'swagger-ui-express';
import swaggerDoc from 'swagger-jsdoc'
import swaggerJSDoc from 'swagger-jsdoc';
import { options } from './routes/apiDocOptions';
// Import Kafka
import kafka, { KafkaClient } from 'kafka-node';

import ExpressAPIServer from './servers/web.server';
// const app = express();

// app.listen(300, ()=>{
//     console.log(settings.environment);
// });

// Constants Declarations
const settings = Settings();
const color = ColorUtilLib();
const debug = DebugUtilLib();
const specs = swaggerDoc(options);
// // Connect to Kafka Server
const cnnKafka = new kafka.KafkaClient({
    kafkaHost: '192.168.3.65:9092'
    // kafkaHost: '4.tcp.ngrok.io:14014'
});
const cnnKafka2 = new kafka.KafkaClient({
    kafkaHost: '192.168.3.65:9092'
    // kafkaHost: '4.tcp.ngrok.io:14014'
});

// Main API Code
(async () => {
    // console.log(await settings.environment());
    // Se obtienen las configuraciones de acuerdo al ambiente (DEV|QA|PROD) 
    global.ENV = await settings.environment();
    const ENV: any = global.ENV;

    const mongo = MongoHelper.getInstance(ENV);
    await mongo.connect(ENV.MONGODB.DATABASE);
    if (mongo.infoConnection.status == 'error') {
        debug.unhandleError(`${color.danger(mongo.infoConnection.msg)}`);
        // Cerrar Conexión de MongoDB
        await mongo.disconnect();
        debug.mongoDB(`${color.mongoDB('MongoDB: ')} ${color.danger('Lo sentimos la conexión al servidor de MongoDB no se pudo realizar')}`);
        process.exit(0);
    }
    // Obtener WhiteList de Dominios Permitidos
    const config: any = await mongo.db.collection('c_config').find({}).toArray();
    /****************************
     * Start Express API Server *
     ****************************/
    const www = ExpressAPIServer.getInstance(ENV.API.PORT, config[0].allowDomains);
    /****************************
    * Kafka *
    ****************************/
    // Consumer Kafka Topic
    const consumerRegistroConsulta: any = new kafka.Consumer(cnnKafka, [
        { topic: 'registro-consulta' }
    ], {});
    consumerRegistroConsulta.on('message', async (data: any) => {
        // console.log("data: %j", data.value.NombreCompleto);
        // console.log(JSON.parse(data.value));
        // console.log(`Nombre Completo: ${JSON.parse(data.value).NombreCompleto}`);        
        // console.log(`El mensaje recibido desd el Producer es`, msg);
        console.log(`${color.info(`Entro al consumer registro-consulta`)}`);
        const {
            NombreCompleto,
            NombreServicio,
            FechaServicio,
            Direccion,
            CodUsuario,
            Sexo,
            FechaNacimiento,
            CodHospital,
            CodServicio,
            Edad,
            // Atendido,
            // FechaAtencion,
            Id,
            CodExpediente
        } = JSON.parse(data.value);

        // // Comprabar si existe persona, Si no existe se inserta        
        // const query = { NombreCompleto, FechaNacimiento, CodUsuario };
        // const update = { $set: { NombreCompleto, FechaNacimiento, CodUsuario, Sexo } };
        // const options = { upsert: true };
        // var resultPaciente: any = await mongo.db.collection('pacientes').updateOne(query, update, options)
        //     .then((result: any) => {
        //         console.log(`resultado:`, result);
        //         return {
        //             pacienteId: result.upsertedId,                    
        //             rowsAffected: result.upsertedId
        //         }
        //     })
        //     .catch((err: any) => {
        //         return err;
        //     });

        var PacienteId: any =
            await mongo.db.collection('pacientes')
                .findOneAndUpdate(
                    { NombreCompleto, FechaNacimiento, CodUsuario }, // Filter or Find Criterio de Busqueda
                    {
                        // $setOnInsert: {
                        //     isVerify: true
                        // },
                        $set: {
                            NombreCompleto, FechaNacimiento, CodUsuario, Sexo
                        }
                    },
                    {
                        upsert: true
                    }
                )
                // .then((result: any) => console.log(result))
                // .then((result: any) => console.log(result.value?._id || result.lastErrorObject?.upserted))
                // .then((result: any) => result.value._id)
                // .then((result: any) => result.lastErrorObject.upserted)
                .then((result: any) => result.value?._id || result.lastErrorObject?.upserted)
                .catch((error: any) => console.log(error));
        // console.log(`id insertado: ${pacienteId}`)
        console.log(`${color.info(`Se inserto correctamente el paciente en MongoDB con id: ${PacienteId}`)}`);
        // var pacienteId = await mongo.db.collection('pacientes').find({`NombreCompleto: {}`})
        // mongo.db.collection('pacientes').findOne({}, function(err, result) {
        //     if (err) throw err;
        //     console.log(result.name);            
        //   });

        // console.log(`${color.info(`Se inserto correctamente la consulta del paciente en MongoDB con id: ${resultPaciente.pacienteId}`)}`);
        var FechaIngreso = new Date();
        const result: any = await mongo.db.collection('consultas').insertOne({
            PacienteId,
            NombreCompleto,
            NombreServicio,
            FechaServicio,
            Direccion,
            CodUsuario,
            Sexo,
            FechaNacimiento,
            CodHospital,
            CodServicio,
            Edad,
            Id,
            CodExpediente,
            Estatus: 'Recibido',
            Tratamiento: '',
            Costo: 0,
            FechaIngreso
        })
            .then((result: any) => {
                return {
                    uid: result.insertedId,
                    rowsAffected: result.insertedCount
                }
            })
            .catch((err: any) => {
                return err;
            });
        // console.log(`${color.info(`Se inserto correctamente la consulta del paciente en MongoDB con id: ${result.uid}`)}`);
        console.log(`${color.info(`Se inserto correctamente la consulta en MongoDB con id: ${result.uid}`)}`);

        // Estableciendo variables para los indicadores
        // var projection = { nom_estab: 1 };
        var resultadoHospital: any = await mongo.db.collection('hospitales').findOne({
            id: parseInt(CodHospital)
        }, { projection: { _id: 0, nom_estab: 1 } })
            ;
        console.log(`NombreHospital`, resultadoHospital.nom_estab);
        const resultIndice: any = await mongo.db.collection('e_indicador').insertOne({
            NombreServicio,
            NombreHospital: resultadoHospital.nom_estab,
            Sexo: Sexo,
            Edad: Edad,
            Estatus: 'Recibido',
            Tratamiento: null,
            Costo: null,
            FechaIngreso
        })
            .then((result: any) => {
                return {
                    uid: result.insertedId,
                    rowsAffected: result.insertedCount
                }
            })
            .catch((err: any) => {
                return err;
            });
        console.log(`${color.info(`Se inserto correctamente el índice en MongoDB con id: ${resultIndice.uid}`)}`);

    });
    // Consumer Kafka Topic
    const consumerSolicitudHospitales: any = new kafka.Consumer(cnnKafka2, [
        { topic: 'solicitud-hospitales' }
    ], {});
    // Producer Kafka Topic
    const producerRespuestaHospitales = new kafka.Producer(cnnKafka2);
    consumerSolicitudHospitales.on('message', async (data: any) => {
        console.log(`${color.info(`Entro al Consumer Solicitud Hospitales`)}`);
        const {
            CodCliente,
            Latitud,
            Longitud,
            Distancia
        } = JSON.parse(data.value);
        var hospitales: any[] = [];
        console.log(`${parseFloat(Longitud)} - ${parseFloat(Latitud)} - ${parseInt(Distancia)}`);
        hospitales = await mongo.db.collection('hospitales').find({
            Location: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [parseFloat(Longitud), parseFloat(Latitud)] // My Location
                    },
                    $maxDistance: parseInt(Distancia),
                    $minDistance: 0
                }
            }
        }).toArray();
        let hospitalesInterface: any[] = [];
        hospitales.map(function (key) {
            hospitalesInterface.push({ CodCliente: parseInt(CodCliente), CodHospital: key.id, NombreHospital: key.nom_estab, Distancia: '', CodigoPostal: key.cod_postal, Entidad: key.entidad, Municipio: key.municipio, Telefono: key.telefono.toString(), Latitud: key.latitud.toString(), Longitud: key.longitud.toString(), LatitudOriginal: Latitud, LongitudOriginal: Longitud });
        });
        // de 
        // [{"_id":"6129b77d00a7f2c2ed0e62a2","id":1304217,"nom_estab":"CENTRO M@DICO LA PRESA","cod_postal":"36090","entidad":"GUANAJUATO","municipio":"Guanajuato","telefono":" ","latitud":21.00420545,"longitud":-101.24699397,"Location":{"type":"Point","coordinates":[-101.24699397,21.00420545]
        //transformar a:
        // {"CodCliente":1,"CodHospital":1,"NombreHospital":"CENTRO MEDICO LA PRESA","Distancia":30,"CodigoPostal":36090,"Entidad":"GUANAJUATO","Municipio":"Guanajuato","Telefono":74445446,"Latitud":"21.00420545","Longitud":"-101.24699397","LatitudOriginal":"21.00420545","LongitudOriginal":"-101.24699397"}
        // console.log(`Hospitales a ${Distancia / 1000} Km: `, hospitales);
        // console.log(`Hospitales a ${Distancia / 1000} Km: `, hospitalesInterface);
        console.log(hospitalesInterface);
        console.log(JSON.stringify(hospitalesInterface));
        // producerRespuestaHospitales.on('ready', () => {
        // setInterval(() => {
        producerRespuestaHospitales.send([
            { topic: 'respuesta-hospitales', messages: JSON.stringify(hospitalesInterface) }
        ],
            (err: any, data: any) => { });
        // }, 5000);
        // });
    });
    /*************************
     * Middlewares de la API *
     *************************/
    // Habilitar Hardenning en Express Server
    www.api.use(helmet());
    // Habilitar Encodig JSON 
    www.api.use(Express.json());
    www.api.use(Express.urlencoded({ extended: true }));
    // Habilitar Express FileUpload
    // www.api.use(fileUpload());
    // Habilitar el Uso de CORS
    const corsOptionsDelegate = (req: any, callback: Function) => {
        let error: any = null;
        let corsOptions: any = {};
        console.log(config[0].allowDomains);
        let isDomainAllowed = config[0].allowDomains.indexOf(req.header('Origin')) !== -1;
        if (isDomainAllowed) {
            // Enable CORS for this request
            corsOptions = {
                origin: true,
                optionsSuccessStatus: 200
            }
        } else {
            // CORS Error for this request
            error = `The CORS policy for this origin ${req.header('Origin')} doesn't allow access from the particular origin.`;
        }
        callback(error, corsOptions);
    }
    www.api.use('/v1/doc', swaggerUI.serve, swaggerUI.setup(specs));
    // www.api.use(cors(corsOptionsDelegate));
    // Habilitar Morgan
    www.api.use(morgan('dev'));
    // Protección contra ataques de denegación de servicio
    www.api.enable('trust proxy')
    const limiter = rateLimit({
        windowMs: 1 * 60 * 1000,
        max: 2000
    });
    www.api.use(limiter);
    // Habilitar Compression
    www.api.use(compression({
        filter: (req: any, res: any) => compression.filter(req, res),
        level: 6
    }));
    // Rutas de la API    
    www.api.use('/v1/demo', DemoRoute);
    www.api.use('/v1/hospital', HospitalRoute);
    // www.api.use('/v1/doc', swaggerUI.serve, swaggerUI.setup(specs));
    // Arrancar Express Server
    www.start(() => {
        debug.express(`${color.express(`Express:`)} ${color.express(`Servidor funcionando correctamente en el puerto ${ENV.API.PORT}`)}`);
    })
    // console.log(await settings.environment());
})();