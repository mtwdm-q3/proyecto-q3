FROM node:12.19

RUN mkdir /usr/src/node
WORKDIR /usr/src/node
COPY . /usr/src/node

RUN npm install
RUN npm install -D nodemon
RUN npm install -g typescript
RUN tsc

EXPOSE 9000

# CMD ["sleep", "3600"]
CMD ["npm", "run", "dev"]