import { Router } from 'express';
import HospitalController from '../controllers/hospital.controller';

const router: Router = Router();
const hospitalController = HospitalController();

/**
 * @swagger
 * /v1/demo/:
 *   get:
 *     summary: End-Point para corroborar que la API se encuentra en línea y funcionando
 *     tags: [API Test]
 *     responses:
 *       200:
 *         description: La solicitud ha tenido éxito.
 *         content:
 *           application/json:
 *             schema: 
 *               type: object
 *               example: 
 *                 success: true
 *                 message: API en línea y funcionando correctamente
 *                 data: {
 *                   datos: [
 *                     ...
 *                   ]
 *                 } 
 */
 router.get('/bylocalizacion/:latitud/:longitud/:distancia', hospitalController.getHospitalListByDistance);
 router.get('/consultas-recibidas/:CodHospital', hospitalController.getConsultasRecibidasByHospital);
//  router.get('/altaconsulta/:consultaid/:tratamiento/:costo', hospitalController.altaConsultaPersona);
 router.post('/altaconsulta', hospitalController.altaConsultaPaciente);
 router.post('/login', hospitalController.login);

export default router;