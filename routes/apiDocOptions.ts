export const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "API Demo",
            version: "1.0.0",
            description: "Documentación de la API Demo"
        },
        host: "basic-auth-server.herokuapp.com",
        schemes: ["http", "https"],
        servers: [
            {
                url: "http://localhost:9000",
                description: "Servidor de Desarrollo"
            },
            {
                url: "http://qa-api.mtwydm.com",
                description: "Servidor de QA"
            },
            {
                url: "http://api.mtwydm.com",
                description: "Servidor de Producción"
            }  
        ]
    },
    apis: ["./routes/*.ts"]
};