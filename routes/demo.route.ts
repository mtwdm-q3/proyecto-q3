import { Router } from 'express';
import DemoController from '../controllers/demo.controller';
import TokenMiddleWare from '../middlewares/token.middleware';

const router: Router = Router();
const demoController = DemoController();
const token = TokenMiddleWare();

/**
 * @swagger
 * /v1/demo/:
 *   get:
 *     summary: End-Point para corroborar que la API se encuentra en línea y funcionando
 *     tags: [API Test]
 *     responses:
 *       200:
 *         description: La solicitud ha tenido éxito.
 *         content:
 *           application/json:
 *             schema: 
 *               type: object
 *               example: 
 *                 success: true
 *                 message: API en línea y funcionando correctamente
 *                 data: {
 *                   datos: [
 *                     ...
 *                   ]
 *                 } 
 */
router.get('/', demoController.testing);

router.post('/createToken', demoController.createToken);

router.post('/endPoint', token.verify, demoController.endPoint);

export default router;