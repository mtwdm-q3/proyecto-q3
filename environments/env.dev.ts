/*******************
 * DEV ENVIRONMENT *
 *******************/

 export default{
    API: {
        ENVIRONMENT: process.env.NODE_ENV, 
        NAME: 'API Proyecto Q3',
        PORT: process.env.API_PORT
    },
    TOKEN: {
        secret: process.env.TOKEN_SECRET
    },
    MONGODB: {
        ENVIRONMENT: process.env.NODE_ENV,
        // HOST: process.env.MONGODB_HOST || 'localhost',
        HOST: process.env.MONGODB_HOST,
        //PORT: process.env.DEV_MONGODB_PORT || 27019,
        // PORT: process.env.MONGODB_PORT || 27019,
        PORT: process.env.MONGODB_PORT,
        // USER_NAME: process.env.DEV_MONGODB_USER_NAME || 'dbo-operator',
        // USER_PASSWORD: process.env.DEV_MONGODB_USER_PASSWORD || 'readwriteoperator',
        // USER_NAME: 'dbo-operator',
        // USER_PASSWORD: 'readwriteoperator',
        // USER_NAME: process.env.MONGODB_USER_NAME || 'dbo-operator',
        USER_NAME: process.env.MONGODB_USER_NAME,
        // USER_PASSWORD: process.env.MONGODB_USER_PASSWORD || 'readwriteoperator',
        USER_PASSWORD: process.env.MONGODB_USER_PASSWORD,
        // USER_NAME: 'dbo-operator',
        // USER_PASSWORD: 'readwriteoperator',
        //DATABASE: process.env.DEV_MONGODB_DATABASE || 'dbMTWyDM'
        DATABASE: process.env.MONGODB_DATABASE
    }
}