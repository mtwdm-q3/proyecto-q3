#!/bin/bash

MONGODB1='MongoDB-Single-Node'

mongo --host ${MONGODB1}:27017 -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD <<EOF
    var cfg = {
        "_id": "rs0",
        "version": 1,
        "members": [
            {
                "_id": 0,
                "host": "${MONGODB1}:27017",
                "priority": 1
            }
        ]
    };
    rs.initiate(cfg);
    rs.reconfig(cfg, {force: true});
    rs.status();

    use dbmtwydm;
    db.createUser({
        user: '$MONGO_USER_NAME',
        pwd: '$MONGO_USER_PASSWORD',
        roles: [
            { role: 'readWrite', db: '$MONGO_DATABASE' },
            { role: 'read', db: 'local' },
            { role: "dbAdmin", db: "$MONGO_DATABASE" },
            { role: 'root', db: '$MONGO_INITDB_DATABASE' }
        ]
    });
    db.createCollection('c_config');
    db.getCollection('c_config').insert(
    {
        "apiKey" : "$API_KEY",
        "apiSecret" : "$API_SECRET",
        "allowDomains" : [ 
            "http://midominio.net", 
            "http://mwtydm.com", 
            "http://03190609b10b.ngrok.io/", 
            "https://www.websiteplanet.com", 
            "http://localhost:3000"
        ]
    });
    db.createCollection('e_indicador');
EOF