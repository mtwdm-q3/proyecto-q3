// Express Imports
import { Request, Response, NextFunction } from 'express';
// MongoDBHelper Import
import MongoHelper from '../helpers/mongo.helper';
// JSON Web Token Import
import jwt from 'jsonwebtoken';
// Debug Util Lib Import
import Debug from '../utils/debug.util';
// Color Util Lib Import
import Color from '../utils/color.util';

export default () => {

    const debug = Debug();
    const color = Color();

    const _getSecretKey = async (apiKey: string) => {
        // Get SecretKey from DataBase
        const mongo = MongoHelper.getInstance(global.ENV);
        return await mongo.db.collection('c_config').findOne({apiKey}, { projection: { _id: 0 , apiSecret: 1 } });
    };

    return {
        create: async (apiKey: string, payload: any) => {
            // Get Secret Key from DataBase
            const result: any = await _getSecretKey(apiKey);
            return new Promise((resolve, reject) => {
                jwt.sign(payload, result.apiSecret, (error: any, token: any) => {
                    if (error) {
                        return reject({
                            status: 'Error',
                            message: `Ocurrio un error no contemplado al intentar generar el Token`,
                            error
                        });
                    }
    
                    return resolve({
                        status: 'Success',
                        message: `Token generado de forma correcta !!!`,
                        token
                    });
                });
            });  
            
        },
        verify: async (req: Request, res: Response, next: NextFunction) => {
            // Obtener el valor del Authorization Header
            const bearerHader = req.headers['authorization'];

            debug.express(color.express('Token Verify ==> '), bearerHader);

            // Obtener ApiKey
            const { apiKey } = req.body;
            // Checar si Bearer es undefined
            if(bearerHader) {
            //if(typeof bearerHader !== 'undefined') {
                const bearer = bearerHader.split(' ');
                const bearerToken = bearer[1];
                const result = await _getSecretKey(apiKey);
                const token: any = jwt.verify(bearerToken, result.apiSecret, (error: any, payload: any) => {
                    if (error) {
                        return {
                            status: 'Forbidden',
                            message: `El token proporcionado, no es un token válido, está caducado, o está mal formado. Favor de verificar`,
                            error
                        }
                    }

                    return {
                        status: 'Success',
                        message: `Token validado de forma correcta`,
                        payload 
                    }
                });

                if (token.status != 'Success') {
                    return res.status(403).json(token);
                }

                req.body.authUser = token.payload;

                if (req.body.authUser.rol != "ADMIN") {
                    return res.status(403).json({
                        status: 'Forbidden',
                        message: 'Lo sentimos no tienenlos permisos suficientes para acceder a este método'
                    })
                }   
                
                next();
            } else {
                return res.status(401).json({
                    status: 'Unauthorized',
                    message: `Es necesario proporcionar un token válido para poder acceder a la API`
                });
            }
        }
    };

}
