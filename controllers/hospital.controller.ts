// Express Import
import { Request, Response, NextFunction } from 'express';
// MongoDBHelper Import
import MongoHelper from '../helpers/mongo.helper';
// Debug Util Lib Import
import Debug from '../utils/debug.util';
// Color Util Lib Import
import Color from '../utils/color.util';

export default () => {
    const debug = Debug();
    const color = Color();

    return {
        getConsultasRecibidasByHospital: async (req: Request, res: Response, next: NextFunction) => {
            const { CodHospital } = req.params;
            console.log(CodHospital);
            const mongo = MongoHelper.getInstance(global.ENV);
            var data: any[] = [];
            data = await mongo.db.collection('consultas').find({
                CodHospital: parseInt(CodHospital), Estatus: 'Recibido'
            }).toArray();
            console.log(data);
            res.status(200).json({
                success: true,
                message: '',
                data
            });
        },
        getHospitalListByDistance: async (req: Request, res: Response, next: NextFunction) => {
            const { latitud, longitud, distancia } = req.params;
            const mongo = MongoHelper.getInstance(global.ENV);
            var data: any[] = [];
            // data = await mongo.db.collection('hospitales').find({ entidad: 'GUANAJUATO' }).toArray();
            console.log(`${parseFloat(longitud)} - ${parseFloat(latitud)} - ${parseInt(distancia)}`);
            data = await mongo.db.collection('hospitales').find({
                Location: {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [parseFloat(longitud), parseFloat(latitud)] // My Location
                        },
                        $maxDistance: parseInt(distancia),
                        $minDistance: 0
                    }
                }
            }).toArray();

            res.status(200).json({
                success: true,
                message: '',
                // data: `datos recibidos: latidud: ${latitud} - longitud: ${longitud} - distancia: ${distancia}`                
                data
            });
        },
        postConsultaPaciente: async (req: Request, res: Response, next: NextFunction) => {
            const { codCliente } = req.body;
        },
        // postPaciente: async (req: Request, res: Response, next: NextFunction) => {
        //     const { nombreCompleto, correo, fechaNacimiento, sexo, edad, codCliente } = req.body;
        //     const mongo = MongoHelper.getInstance(global.ENV);
        //     const result: any = await mongo.db.collection('pacientes').insertOne({
        //         nombreCompleto, correo, fechaNacimiento, sexo, edad, codCliente 
        //     })
        //         .then((result: any) => {
        //             return {
        //                 uid: result.insertedId,
        //                 rowsAffected: result.insertedCount
        //             }
        //         })
        //         .catch((err: any) => {
        //             return err;
        //         });
        //     res.status(201).json({
        //         uid: result.uid,
        //         nombreCompleto, correo, fechaNacimiento, sexo, edad, codCliente,
        //         rowsAffected: result.rowsAffected
        //     });
        // },
        altaConsultaPaciente: async (req: Request, res: Response, next: NextFunction) => {
            const { Id, Tratamiento, Costo } = req.body;
            const mongo = MongoHelper.getInstance(global.ENV);
            var pacienteId: any =
                await mongo.db.collection('consultas')
                    .findOneAndUpdate(
                        { Id }, // Filter or Find Criterio de Busqueda
                        {
                            $set: {
                                Tratamiento, Costo, Estatus: 'Atendido'
                            }
                        },
                        {
                            upsert: true
                        }
                    )
                    //         // .then((result: any) => console.log(result))
                    //         // .then((result: any) => console.log(result.value?._id || result.lastErrorObject?.upserted))
                    //         // .then((result: any) => result.value._id)
                    //         // .then((result: any) => result.lastErrorObject.upserted)
                    .then((result: any) => result.value?._id || result.lastErrorObject?.upserted)
                    .catch((error: any) => console.log(error));
            res.status(201).json({
                Id,
                Tratamiento,
                Costo,
                FechaAtencion: new Date().toLocaleString(),
            });
        },
        login: async (req: Request, res: Response, next: NextFunction) => {
            const { Usuario, Password } = req.body;
            const mongo = MongoHelper.getInstance(global.ENV);
            var usuario: any =
                await mongo.db.collection('usuarios')
                    .findOne(
                        { CodHospital: parseInt(Usuario), Password, Activo: true }, // Filter or Find Criterio de Busqueda
                    );
            console.log(`usuario`, usuario);
            res.status(201).json({
                id: usuario._id,
            });
        }
    }
};