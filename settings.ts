export default () => {
    return {
        environment: async () => {
            let env: any = {};
            // console.log("Entro antes del switch...");
            // console.log(process.env.NODE_ENV);
            switch(process.env.NODE_ENV?.trim()){
                // case 'PROD': env = await import('./environments/env.prod');
                //     break;
                // case 'QA': env = await import('./environments/env.qa');
                //     break;
                case 'DEV': 
                    // console.log("Entro al dev switch...");
                    env = await import('./environments/env.dev');
                    break;
            }
            return env.default;
        }
    }
}